'use strict'

const AWS = require('aws-sdk');
const s3 = new AWS.S3();
const Moment = require('moment');
const fileType = require('file-type');
const sha1 = require('sha1');

exports.handler = async (event, context, callback) => {
  console.log("in function");
  console.log(event.body);
  let encodedImage = JSON.parse(event.body).image;
  console.log(encodedImage);

  let decodedImage = Buffer.from(encodedImage.replace(/^data:image\/\w+;base64,/, ""), 'base64');
  console.log(decodedImage);


  let fileMime = fileType(decodedImage);

  if (fileMime == null) {
    return context.fail("Not a detectable image")
  }

  let file = getFile(fileMime, decodedImage);
  console.log(file);
  let params = file.params

  console.log(params);

  let s3Response = s3.putObject(params).promise();
  try {
    let result = await s3Response;
    console.log('succesfully uploaded the image!');
    console.log(result);
    console.log('Success');
    let response = {
        statusCode: 200,
        body: JSON.stringify({imageURL: file.uploadFile.full_path})
    }
    return response;
    } catch(err) {
      console.log("error")
      console.log(err); // TypeError: failed to fetch
  };
};

let getFile = function(fileMime, buffer) {
  // Get the file extension
  let fileExt = fileMime.ext;
  let folder = 'ingress';
  let now = Moment().format('YYYY-MM-DDHH-mm-ss');

  let filePath = folder + '/'
  let fileName = now + '.' + fileExt;
  let fileFullName = filePath + fileName
  let fileFullPath = process.env.INGRESS_BUCKET + '/' + fileFullName

  console.log("Before making params");

  let params = {
      Bucket: process.env.INGRESS_BUCKET,
      Key: fileFullName,
      Body: buffer,
      ContentType: fileMime.mime
  }

  console.log("before making upload file");

  let uploadFile = {
    size: buffer.toString('ascii').length,
    type: fileMime.mime,
    name: fileName,
    full_path: fileFullPath
  }

  return {
    'params': params,
    'uploadFile': uploadFile
  }

}
