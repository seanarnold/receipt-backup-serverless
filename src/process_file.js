'use strict'

const AWS = require('aws-sdk');
const dynamodb = new AWS.DynamoDB.DocumentClient();
const s3 = new AWS.S3();
const textract = new AWS.Textract();
const fileType = require('file-type');
let objectKey;

exports.handler = async (event, context, callback) => {
  objectKey = event.Records[0].s3.object.key;
  var params = {
    Document: {
      S3Object: {
        Bucket: process.env.INGRESS_BUCKET,
        Name: objectKey
      }
    }
  };

  let data = await textract.detectDocumentText(params).promise();
  var foundText = data.Blocks.map(x => {
    if (x.BlockType == 'WORD') {
      return x.Text;
    } else {
      return null;
    }
  }).filter(function (el) {
    return el != null;
  });

  console.log(foundText);
  let itemParams = await saveDataToDynamo(foundText)
  const response = {
    statusCode: 200,
    body: JSON.stringify(itemParams.Item),
  };
  return callback(null, response);
}

async function saveDataToDynamo(foundText) {
  const timestamp = new Date().toISOString();
  console.log(`objectkey is ${objectKey}`);
  const params = {
    TableName: process.env.DYNAMODB_TABLE,
    Item: {
      imageKey: objectKey,
      imageBucket: process.env.INGRESS_BUCKET,
      date: timestamp,
      tags: foundText
    }
  }

  console.log(`params to log are ${JSON.stringify(params)}`)

  try {
    console.log("about to put")
    await dynamodb.put(params).promise();
    return params
  } catch(error) {
    console.log("error!")
    console.error(error);
    return false;
  }
}
