# Serverless Receipt Backup

The idea is to create an endpoint where I can backup my paper receipts to the cloud.

1. Create function which takes an image via HTTP/S endpoint.
2. Backup image to S3 via Lambda function
3. Lambda function to pull image from S3 and process, including reading all text. Save path to S3 and associated text to DB
4. Create UI which can search for text. Related receipt data will be shown as search results
